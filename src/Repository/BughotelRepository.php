<?php

namespace App\Repository;

use App\Entity\Bughotel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Bughotel|null find($id, $lockMode = null, $lockVersion = null)
 * @method Bughotel|null findOneBy(array $criteria, array $orderBy = null)
 * @method Bughotel[]    findAll()
 * @method Bughotel[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BughotelRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Bughotel::class);
    }

    // /**
    //  * @return Bughotel[] Returns an array of Bughotel objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Bughotel
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
