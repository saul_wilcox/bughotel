<?php

namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BughotelRepository;

class MapController extends AbstractController
{
    protected $bughotelRepository;

    public function __construct(
        BughotelRepository $bughotelRepository
    ){
        $this->bughotelRepository = $bughotelRepository;
    }

    /**
     * @Route("/map", name="map")
     */
    public function index()
    {
	$hotels = [];
        
        $hotels = $this->bughotelRepository->findBy([]);

        return $this->render('map.html.twig', [
            'content' => '-content-',
            'hotels' => $hotels
        ]);
    }
}
