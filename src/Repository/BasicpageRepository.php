<?php

namespace App\Repository;

use App\Entity\Basicpage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Basicpage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Basicpage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Basicpage[]    findAll()
 * @method Basicpage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BasicpageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Basicpage::class);
    }

    // /**
    //  * @return Basicpage[] Returns an array of Basicpage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Basicpage
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
